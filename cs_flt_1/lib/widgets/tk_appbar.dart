import 'package:cs_flt_1/core/notifications.dart';
import 'package:cs_flt_1/pages/pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:provider/provider.dart';

class TKAppBar extends StatefulWidget implements PreferredSizeWidget {
  const TKAppBar({Key? key}) : super(key: key);

  @override
  _TKAppBarState createState() => _TKAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(120);
}

class _TKAppBarState extends State<TKAppBar> {
  @override
  Widget build(BuildContext context) {
    const radius = Radius.circular(40);
    const BorderRadius appBarRadius = BorderRadius.only(
      bottomLeft: radius,
      bottomRight: radius,
    );
    final appBarTheme = Theme.of(context).appBarTheme;

    return IconTheme(
      data: appBarTheme.iconTheme!,
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: appBarRadius,
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              spreadRadius: 1.0,
              blurRadius: 5.0,
              offset: Offset(0.0, 4.0),
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: appBarRadius,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset(
                "assets/images/appbar-bg.png",
                fit: BoxFit.cover,
              ),
              Container(
                color: appBarTheme.color?.withAlpha(200),
              ),
              SafeArea(
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "تیکوماتیک",
                            style: appBarTheme.titleTextStyle,
                          ),
                          Consumer<NotificationsProvider>(
                            builder: (context, notifp, _) => GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            NotificationsPage()));
                              },
                              child: Icon(
                                Icons.notifications,
                                color: (notifp.messages.isNotEmpty &&
                                        !notifp.isRead)
                                    ? Colors.red
                                    : null,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0)
                            .add(const EdgeInsets.symmetric(horizontal: 8.0)),
                        child: TextField(
                          // style: const TextStyle(
                          // ),
                          onSubmitted: (searchStr) {
                            debugPrint(searchStr);
                          },
                          decoration: InputDecoration(
                            fillColor: Colors.white,
                            filled: true,
                            hintText: "جستجو در محصولات...",
                            hintStyle: const TextStyle(
                              color: Colors.grey,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                              borderSide: BorderSide.none,
                            ),
                            contentPadding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            suffixIcon: const Icon(Icons.search),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
