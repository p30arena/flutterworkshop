import 'package:flutter/material.dart';

const MaterialColor tkcolor = MaterialColor(_tkcolorPrimaryValue, <int, Color>{
  50: Color(0xFFF8E9EF),
  100: Color(0xFFEDC8D8),
  200: Color(0xFFE1A3BE),
  300: Color(0xFFD57EA3),
  400: Color(0xFFCC6290),
  500: Color(_tkcolorPrimaryValue),
  600: Color(0xFFBD3F74),
  700: Color(0xFFB53769),
  800: Color(0xFFAE2F5F),
  900: Color(0xFFA1204C),
});
const int _tkcolorPrimaryValue = 0xFFC3467C;

const MaterialColor tkcolorAccent =
    MaterialColor(_tkcolorAccentValue, <int, Color>{
  100: Color(0xFFFFDBE7),
  200: Color(_tkcolorAccentValue),
  400: Color(0xFFFF75A1),
  700: Color(0xFFFF5C8F),
});
const int _tkcolorAccentValue = 0xFFFFA8C4;
