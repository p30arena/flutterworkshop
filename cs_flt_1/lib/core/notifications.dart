import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';

class NotificationsProvider extends ChangeNotifier {
  List<RemoteMessage> _messages = [];
  bool _isRead = true;

  List<RemoteMessage> get messages => [..._messages];
  bool get isRead => _isRead;

  NotificationsProvider() {
    FirebaseMessaging.onMessage.listen(_onMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenedApp);
  }

  _onMessage(RemoteMessage message) {
    _messages.add(message);
    _isRead = false;
    notifyListeners();
  }

  _onMessageOpenedApp(RemoteMessage message) {
    _messages.add(message);
    _isRead = false;
    notifyListeners();
  }

  void markAsRead() {
    _isRead = true;
    notifyListeners();
  }
}
