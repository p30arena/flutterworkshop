import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cs_flt_1/core/models/user.dart';
import 'package:http/http.dart' as http;

List<User>? _cachedUsersList;
DateTime? _cult;

void fetchUserData(Completer c) async {
  late List<User> data;
  try {
    if (_cachedUsersList == null ||
        (_cult != null && DateTime.now().difference(_cult!).inMinutes > 2)) {
      final res =
          await http.get(Uri.parse("https://reqres.in/api/users?page=1"));
      final parsedBody = json.decode(res.body);
      data = (parsedBody["data"] as List)
          .map<User>((data) => User.fromJson(data))
          .toList();

      _cachedUsersList = data;
      _cult = DateTime.now();
    } else {
      data = _cachedUsersList!;
    }

    c.complete(data);
  } catch (e) {
    if (e is SocketException) {
      c.completeError("no network");
    } else if (e is TimeoutException) {
      c.completeError("please check your network connection");
    } else {
      c.completeError(e.toString());
    }
  }
}
