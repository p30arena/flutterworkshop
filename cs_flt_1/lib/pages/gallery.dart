import 'dart:io';

import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

class GalleryPage extends StatefulWidget {
  final String title;

  const GalleryPage({
    Key? key,
    this.title: "null",
  }) : super(key: key);

  @override
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  Future<List<File>> listFiles() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    final subDirPath = p.join(appDocDir.path, './images');

    final subDir = Directory(subDirPath);

    if (await subDir.exists()) {
      return (await subDir.list().toList())
          .map((fse) => File(fse.path))
          .toList();
    } else {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder<List<File>>(
        future: listFiles(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return GridView.count(
              crossAxisCount: 3,
              children: snapshot.data!.map((f) => Image.file(f)).toList(),
            );
          } else {
            return const Center(
              child: Text("No Images"),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final PickedFile? pickedFile = await ImagePicker().getImage(
            source: ImageSource.camera,
          );

          if (pickedFile != null) {
            final pf = File(pickedFile.path);
            Directory appDocDir = await getApplicationDocumentsDirectory();

            // appDocDir.path + '/' + pf.path;
            final subDir = p.join(appDocDir.path, './images');
            try {
              await Directory(subDir).create(recursive: true);
            } catch (e) {}

            final destFile = File(p.join(
                subDir,
                DateTime.now().millisecondsSinceEpoch.toString() +
                    p.basename(pf.path)));

            await destFile.create();
            await destFile.writeAsBytes(await pf.readAsBytes());

            setState(() {});
          }
        },
        child: Text("pick"),
      ),
    );
  }
}
