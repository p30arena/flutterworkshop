import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class ShopHomePage extends StatefulWidget {
  const ShopHomePage();

  @override
  _ShopHomePageState createState() => _ShopHomePageState();
}

class _ShopHomePageState extends State<ShopHomePage> {
  final PageController pc = PageController(
    initialPage: 0,
  );

  int currentIndex = 0;

  @override
  void initState() {
    super.initState();

    pc.addListener(() {
      if (pc.page == pc.page!.toInt()) {
        setState(() {
          currentIndex = pc.page!.toInt();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: PageView(
        controller: pc,
        children: [
          ListView(
            children: [
              Container(
                color: Colors.red,
                height: 200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ...List<Card>.generate(
                      4,
                      (index) => Card(
                        elevation: 8,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Container(
                          width: 80,
                          height: 80,
                          child: ClipRRect(
                            child: Stack(
                              children: [
                                Image.network(
                                  "https://media.self.com/photos/5af5ff7153400e2ecd4ee83f/master/pass/makeup-ingredients.jpg",
                                  width: 80,
                                  height: 80,
                                  fit: BoxFit.cover,
                                ),
                                Container(
                                  width: 80,
                                  height: 80,
                                  color: Colors.black.withAlpha(150),
                                ),
                                Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        (index + 1).toString(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        "test",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ...List.generate(
                  40,
                  (index) => ListTile(
                          title: Text(
                        index.toString(),
                      ))),
            ],
          ),
          Center(
            child: Text("1"),
          ),
          Center(
            child: Text("2"),
          ),
          Center(
            child: Text("3"),
          ),
        ],
      ),
      // backgroundColor: Colors.blue,
      bottomNavigationBar: MyBottomAppbar(
        currentIndex: currentIndex,
        onTap: (index) {
          // pc.jumpToPage(index);
          pc.animateToPage(
            index,
            duration: const Duration(
              milliseconds: 500,
            ),
            curve: Curves.ease,
          );
        },
        items: [
          MyBottomAppbarItem(
            icon: Icon(Icons.home),
            label: "خانه",
          ),
          MyBottomAppbarItem(
            icon: Icon(Icons.person),
            label: "پروفایل",
          ),
          MyBottomAppbarItem(
            // icon: SvgPicture.asset(
            //   "assets/icons/category.svg",
            //   color: Colors.grey[700],
            //   width: 80,
            //   alignment: Alignment.center,
            //   fit: BoxFit.fitHeight,
            // ),
            icon: Icon(Icons.list),
            label: "دسته بندی",
          ),
          MyBottomAppbarItem(
            icon: Icon(Icons.shopping_bag),
            label: "سبد خرید",
          ),
        ],
      ),
      extendBody: true,
    );
  }
}

class MyBottomAppbarItem {
  final Widget icon;
  final String label;

  MyBottomAppbarItem({
    required this.icon,
    required this.label,
  });
}

class MyBottomAppbar extends StatefulWidget {
  final List<MyBottomAppbarItem> items;
  final Function(int) onTap;
  final int currentIndex;

  const MyBottomAppbar({
    Key? key,
    required this.items,
    required this.onTap,
    required this.currentIndex,
  }) : super(key: key);

  @override
  State<MyBottomAppbar> createState() => _MyBottomAppbar();
}

class _MyBottomAppbar extends State<MyBottomAppbar> {
  late int navIndex = 0;

  @override
  void initState() {
    super.initState();

    navIndex = widget.currentIndex;
  }

  @override
  void didUpdateWidget(covariant MyBottomAppbar oldWidget) {
    super.didUpdateWidget(oldWidget);

    navIndex = widget.currentIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 8.0,
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    spreadRadius: 1.0,
                    blurRadius: 8.0,
                    offset: Offset(0.0, 4.0),
                  ),
                ],
                borderRadius: BorderRadius.circular(200),
              ),
              child: ClipPath(
                clipper: BAClipper(),
                child: Container(
                  height: 72,
                  padding: EdgeInsets.symmetric(horizontal: 16.0)
                      .add(EdgeInsets.only(right: 36.0)),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(200),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: List<BNButton>.generate(
                        widget.items.length,
                        (i) => BNButton(
                              index: i,
                              selectedIndex: navIndex,
                              onTap: (ti) {
                                setState(() {
                                  navIndex = ti;
                                });
                                widget.onTap(ti);
                              },
                              icon: widget.items[i].icon,
                              label: widget.items[i].label,
                            )),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 8,
          ),
          GestureDetector(
            onTap: () {},
            // shape: const CircleBorder(),
            child: Container(
              padding: const EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                gradient: LinearGradient(
                  // begin: Alignment(0, 0),
                  // end: Alignment(0, 50),
                  // stops: [0.2, 1],
                  colors: [
                    Colors.purple[700]!,
                    Colors.purple,
                  ],
                ),
              ),
              child: const Icon(
                Icons.more_vert,
                size: 40,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BAClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path main = Path();
    main.moveTo(0, 0);
    main.addRect(
        Rect.fromLTWH(-16.0, -8.0, size.width + 32.0, size.height + 16.0));

    Path slice = Path();
    slice.moveTo(size.width, size.height);
    slice.addOval(Rect.fromCircle(
      center: Offset(size.width - 8.0, size.height / 2),
      radius: 40,
    ));

    return Path.combine(PathOperation.difference, main, slice);
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }
}

class BNButton extends StatelessWidget {
  final Widget icon;
  final String label;
  final Function(int) onTap;
  final int index;
  final int selectedIndex;

  const BNButton({
    Key? key,
    required this.icon,
    required this.label,
    required this.onTap,
    required this.index,
    required this.selectedIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        IconButton(
          onPressed: () => onTap(index),
          padding: const EdgeInsets.all(0.0),
          icon: icon,
          iconSize: 30,
          color: selectedIndex == index ? Colors.blue : Colors.grey[700],
        ),
        Text(label),
      ],
    );
  }
}
