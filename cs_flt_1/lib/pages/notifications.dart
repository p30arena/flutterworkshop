import 'package:cs_flt_1/core/notifications.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NotificationsPage extends StatefulWidget {
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  initState() {
    super.initState();

    // Future(() {
    //   Provider.of<NotificationsProvider>(context, listen: false).markAsRead();
    // });

    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      Provider.of<NotificationsProvider>(context, listen: false).markAsRead();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Consumer<NotificationsProvider>(
        builder: (context, notifp, _) => ListView(
          children: notifp.messages
              .map((rm) => Text(rm.notification!.title!))
              .toList(),
        ),
      ),
    );
  }
}
