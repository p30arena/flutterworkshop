import 'package:flutter/material.dart';

class CheckoutFormPage extends StatefulWidget {
  const CheckoutFormPage({Key? key}) : super(key: key);

  @override
  _CheckoutFormPageState createState() => _CheckoutFormPageState();
}

class _CheckoutFormPageState extends State<CheckoutFormPage> {
  GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController usernameController = TextEditingController();

  String gender = "none";

  bool isStudent = false;
  bool isMarried = false;
  bool isGenius = false;

  int age = 15;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        key: formKey,
        child: ListView(
          children: [
            TextFormField(
              controller: usernameController,
              // validator: (value) {
              //   if(value?.isEmpty == true) {
              //     return "باید نام کاربری وارد شود";
              //   }

              //   if (RegExp(r"^\w+$").hasMatch(value ?? "")) {
              //     return null; // valid
              //   } else {
              //     return "باید نام کاربری از حروف و اعداد انگلیسی باشد"; // has error
              //   }
              // },
              decoration: InputDecoration(
                hintText: "نام کاربری",
              ),
            ),
            FormField<String>(
                initialValue: gender,
                validator: (v) {
                  if (v == "none") {
                    return "باید جنسیت تعیین شود";
                  } else {
                    return null;
                  }
                },
                builder: (ffs) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RadioListTile<String>(
                        value: "female",
                        groupValue: gender,
                        onChanged: (v) {
                          ffs.didChange(v);
                          gender = v!;
                        },
                        title: Text("زن"),
                      ),
                      RadioListTile<String>(
                        value: "male",
                        groupValue: gender,
                        onChanged: (v) {
                          ffs.didChange(v);
                          gender = v!;
                        },
                        title: Text("مرد"),
                      ),
                      RadioListTile<String>(
                        value: "none",
                        groupValue: gender,
                        onChanged: (v) {
                          ffs.didChange(v);
                          gender = v!;
                        },
                        title: Text("هیچ کدام"),
                      ),
                      if (ffs.hasError)
                        ListTile(
                          title: Text(
                            ffs.errorText!,
                            style:
                                Theme.of(context).textTheme.caption!.copyWith(
                                      color: Colors.red,
                                    ),
                          ),
                        ),
                    ],
                  );
                }),
            CheckboxListTile(
              value: isStudent,
              onChanged: (v) {
                setState(() {
                  isStudent = v!;
                });
              },
              title: Text("دانشجو هستم"),
            ),
            CheckboxListTile(
              value: isMarried,
              onChanged: (v) {
                setState(() {
                  isMarried = v!;
                });
              },
              title: Text("متاهل هستم"),
            ),
            CheckboxListTile(
              value: isGenius,
              onChanged: (v) {
                setState(() {
                  isGenius = v!;
                });
              },
              title: Text("نابغه هستم"),
            ),
            Row(
              children: [
                Expanded(
                  child: Slider(
                    min: 15,
                    max: 250,
                    divisions: 235,
                    value: age.toDouble(),
                    onChanged: (v) {
                      setState(() {
                        age = v.toInt();
                      });
                    },
                    label: age.toString(),
                  ),
                ),
                Text(age.toString()),
              ],
            ),
            ElevatedButton(
              onPressed: () {
                debugPrint(formKey.currentState!.validate().toString());
              },
              child: Text("Submit"),
            ),
          ],
        ),
      ),
    );
  }
}
