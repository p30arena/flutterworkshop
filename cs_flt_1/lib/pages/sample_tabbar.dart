import 'package:flutter/material.dart';

// class SampleTabbarPage extends StatelessWidget {
//   const SampleTabbarPage({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     // final TabController tbc = TabController(vsync: this);

//     return DefaultTabController(
//       length: 2,
//       child: Scaffold(
//         appBar: AppBar(
//           bottom: TabBar(
//             // controller: tbc,
//             tabs: [
//               Tab(
//                 child: Text("1"),
//               ),
//               Tab(
//                 child: Text("2"),
//               ),
//             ],
//           ),
//         ),
//         body: TabBarView(
//           // controller: tbc,
//           children: [
//             Container(
//               child: Text("1"),
//             ),
//             Container(
//               child: Text("2"),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

class SampleTabbarPage extends StatefulWidget {
  const SampleTabbarPage({Key? key}) : super(key: key);

  @override
  _SampleTabbarPageState createState() => _SampleTabbarPageState();
}

class _SampleTabbarPageState extends State<SampleTabbarPage>
    with SingleTickerProviderStateMixin {
  late TabController tbc;

  @override
  initState() {
    super.initState();

    tbc = TabController(vsync: this, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: TabBar(
          controller: tbc,
          tabs: [
            Tab(
              child: Text("1"),
            ),
            Tab(
              child: Text("2"),
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: tbc,
        children: [
          Container(
            child: Text("1"),
          ),
          Container(
            child: Text("2"),
          ),
        ],
      ),
    );
  }
}
