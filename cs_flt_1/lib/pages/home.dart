import 'dart:async';

import 'package:cs_flt_1/core/api/user.dart';
import 'package:cs_flt_1/core/models/user.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final Completer<List<User>> c = Completer();

  @override
  void initState() {
    super.initState();

    fetchUserData(c);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder<List<User>>(
        future: c.future,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text(snapshot.error as String));
          }

          if (snapshot.connectionState != ConnectionState.done) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return ListView(
            children:
                snapshot.data!.map<Text>((u) => Text(u.firstName)).toList(),
          );
        },
      ),
    );
  }
}
