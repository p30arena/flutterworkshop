import 'package:cs_flt_1/core/colors.dart';
import 'package:cs_flt_1/pages/gallery.dart';
import 'package:cs_flt_1/widgets/widgets.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart' as intl;
import 'package:jalali_calendar/jalali_calendar.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:test_plugin/test_plugin.dart';

import 'pages.dart';

class ShopHomePage extends StatefulWidget {
  const ShopHomePage();

  @override
  _ShopHomePageState createState() => _ShopHomePageState();
}

class _ShopHomePageState extends State<ShopHomePage> {
  final PageController pc = PageController(
    initialPage: 0,
  );

  int currentIndex = 0;

  GlobalKey<ScaffoldState> scKey = GlobalKey();

  @override
  void initState() {
    super.initState();

    pc.addListener(() {
      if (pc.page == pc.page!.toInt()) {
        setState(() {
          currentIndex = pc.page!.toInt();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final AppLocalizations l10n = AppLocalizations.of(context)!;

    List<MyBottomAppbarItem> bottomAppbarItems = [
      MyBottomAppbarItem(
        icon: Icon(Icons.home),
        label: l10n.home,
      ),
      MyBottomAppbarItem(
        icon: Icon(Icons.person),
        label: "پروفایل",
      ),
      MyBottomAppbarItem(
        // icon: SvgPicture.asset(
        //   "assets/icons/category.svg",
        //   color: Colors.grey[700],
        //   width: 80,
        //   alignment: Alignment.center,
        //   fit: BoxFit.fitHeight,
        // ),
        icon: Icon(Icons.list),
        label: "دسته بندی",
      ),
      MyBottomAppbarItem(
        icon: Icon(Icons.shopping_bag),
        label: "سبد خرید",
      ),
    ];

    if (Directionality.of(context) == TextDirection.rtl) {
      bottomAppbarItems = bottomAppbarItems.reversed.toList();
    }

    List<Widget> pageViewChildren = [
      ListView(
        children: [
          Container(
            height: 200,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ...List<Container>.generate(
                  4,
                  (index) => Container(
                    width: 80,
                    height: 80,
                    decoration: const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 1.0,
                          blurRadius: 8.0,
                          offset: Offset(0.0, 4.0),
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Image.network(
                            "https://media.self.com/photos/5af5ff7153400e2ecd4ee83f/master/pass/makeup-ingredients.jpg",
                            width: 80,
                            height: 80,
                            fit: BoxFit.cover,
                          ),
                          Container(
                            width: 80,
                            height: 80,
                            color: Colors.black.withAlpha(150),
                          ),
                          Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  (index + 1).toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                Text(
                                  "test",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          // Builder(builder: (context) {
          //   return ElevatedButton(
          //       onPressed: () {
          //         // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          //         //   content: Text("Hello"),
          //         //   duration: Duration(seconds: 1),
          //         //   action: SnackBarAction(label: "Ok", onPressed: () {}),
          //         // ));

          //         Scaffold.of(context).showBottomSheet((context) => Container(
          //               color: Colors.red,
          //             ));

          //         // showBottomSheet(
          //         //     context: context,
          //         //     builder: (context) => Container(
          //         //           color: Colors.red,
          //         //         ));
          //       },
          //       child: Text("show snack"));
          // }),
          ElevatedButton(
              onPressed: () {
                scKey.currentState!.showBottomSheet((context) => Container(
                      color: Colors.red,
                    ));
              },
              child: Text("show snack")),
          ElevatedButton(
              onPressed: () async {
                debugPrint((await Permission.storage.isGranted).toString());
                if ((await Permission.storage.isGranted) == false) {
                  final status = await Permission.storage.request();

                  debugPrint(status.toString());
                }
              },
              child: Text("grant storage permission")),
          ElevatedButton(
              onPressed: () async {
                Navigator.pushNamed(context, "form");
              },
              child: Text("show form")),
          ElevatedButton(
              onPressed: () async {
                Navigator.pushNamed(context, "sample_tabbar");
              },
              child: Text("tabbar sample")),
          ElevatedButton(
              onPressed: () async {
                Navigator.pushNamed(context, "sample_sliver");
              },
              child: Text("sliver sample")),
          ElevatedButton(
              onPressed: () async {
                Navigator.pushNamed(context, "sample_anim");
              },
              child: Text("animation sample")),
          ElevatedButton(
              onPressed: () async {
                Navigator.pushNamed(context, "sample_hero");
              },
              child: Text("hero sample")),
          ElevatedButton(
              onPressed: () async {
                debugPrint((await TestPlugin.batteryLevel).toString());
              },
              child: Text("print battery level")),
          Container(
            // height: 160,
            color: Colors.grey[300],
            child: GridView.count(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              crossAxisCount: 4,
              // padding: const EdgeInsets.all(8.0),
              mainAxisSpacing: 1.5,
              crossAxisSpacing: 1.5,
              children: List.generate(
                  8,
                  (index) => Container(
                        alignment: Alignment.center,
                        color: Colors.white,
                        child: Text(index.toString()),
                      )),
            ),
          ),
          DataTable(
            columns: [
              DataColumn(label: Text("1")),
              DataColumn(label: Text("2")),
              DataColumn(label: Text("3")),
            ],
            rows: [
              DataRow(
                cells: [
                  DataCell(Text("C1")),
                  DataCell(Text("C2")),
                  DataCell(Text("C3")),
                ],
              ),
            ],
          ),
          ...List.generate(
              40,
              (index) => ListTile(
                      title: Text(
                    index.toString(),
                  ))),
        ],
      ),
      Center(
        child: Text("1"),
      ),
      Center(
        child: Text("2"),
      ),
      Center(
        child: Text("3"),
      ),
    ];

    // if (Directionality.of(context) == TextDirection.rtl) {
    //   pageViewChildren = pageViewChildren.reversed.toList();
    // }

    return Scaffold(
      key: scKey,
      appBar: TKAppBar(),
      body: PageView(
        controller: pc,
        children: pageViewChildren,
      ),
      // backgroundColor: Colors.blue,
      bottomNavigationBar: MyBottomAppbar(
        currentIndex: Directionality.of(context) == TextDirection.rtl
            ? 3 - currentIndex
            : currentIndex,
        onTap: (index) {
          if (Directionality.of(context) == TextDirection.rtl) {
            index = 3 - index;
          }

          pc.animateToPage(
            index,
            duration: const Duration(
              milliseconds: 500,
            ),
            curve: Curves.ease,
          );
        },
        items: bottomAppbarItems,
      ),
      extendBody: true,
    );
  }
}

class MyBottomAppbarItem {
  final Widget icon;
  final String label;

  MyBottomAppbarItem({
    required this.icon,
    required this.label,
  });
}

class MyBottomAppbar extends StatefulWidget {
  final List<MyBottomAppbarItem> items;
  final Function(int) onTap;
  final int currentIndex;

  const MyBottomAppbar({
    Key? key,
    required this.items,
    required this.onTap,
    required this.currentIndex,
  }) : super(key: key);

  @override
  State<MyBottomAppbar> createState() => _MyBottomAppbar();
}

class _MyBottomAppbar extends State<MyBottomAppbar> {
  late int navIndex = 0;

  @override
  void initState() {
    super.initState();

    navIndex = widget.currentIndex;
  }

  @override
  void didUpdateWidget(covariant MyBottomAppbar oldWidget) {
    super.didUpdateWidget(oldWidget);

    navIndex = widget.currentIndex;
  }

  @override
  Widget build(BuildContext context) {
    final double fltBtnSize = 55.0;
    final size = MediaQuery.of(context).size;

    return Directionality(
      textDirection: TextDirection.ltr,
      child: Container(
        margin: const EdgeInsets.symmetric(
          horizontal: 16.0,
          vertical: 8.0,
        ),
        child: Row(
          children: [
            Expanded(
              child: CustomPaint(
                painter: BAPaint(),
                child: Container(
                  height: fltBtnSize * 1.2,
                  padding: EdgeInsets.symmetric(horizontal: 16.0)
                      .add(EdgeInsets.only(right: 36.0)),
                  // decoration: BoxDecoration(
                  //   color: Colors.white,
                  //   borderRadius: BorderRadius.circular(200),
                  // ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: List<BNButton>.generate(
                        widget.items.length,
                        (i) => BNButton(
                              index: i,
                              selectedIndex: navIndex,
                              onTap: (ti) {
                                setState(() {
                                  navIndex = ti;
                                });
                                widget.onTap(ti);
                              },
                              icon: widget.items[i].icon,
                              label: widget.items[i].label,
                            )),
                  ),
                ),
              ),
            ),
            // SizedBox(
            //   width: 8,
            // ),
            GestureDetector(
              onTap: () async {
                if (Localizations.localeOf(context).languageCode == "fa") {
                  final date = await jalaliCalendarPicker(
                    context: context,
                    convertToGregorian: true,
                  );

                  if (date != null) {
                    debugPrint(date);

                    final dp = date.split(" ").first.split("-");

                    final dt = DateTime(
                        int.parse(dp[0]), int.parse(dp[1]), int.parse(dp[2]));

                    debugPrint(intl.DateFormat.yMd(
                            Localizations.localeOf(context).toString())
                        .format(dt));
                  }

                  // DatePicker.showDatePicker(
                  //   context,
                  //   onConfirm: (y, m, d) {
                  //     final dt = DateTime(y!, m!, d!);
                  //     debugPrint(dt.toString());
                  //   },
                  // );

                  // debugPrint(intl.DateFormat.yMd(
                  //         Localizations.localeOf(context).toString())
                  //     .format(DateTime.now()));
                } else {
                  showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime.now(),
                      lastDate: DateTime.now().add(Duration(days: 30)));
                }
              },
              // shape: const CircleBorder(),
              child: GestureDetector(
                onTap: () async {
                  // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  //   content: Text("Hello"),
                  //   duration: Duration(seconds: 1),
                  //   action: SnackBarAction(label: "Ok", onPressed: () {}),
                  // ));

                  // showBottomSheet(
                  //     context: context,
                  //     builder: (context) => Container(
                  //           color: Colors.red,
                  //         ));

                  // final result = await showDialog<bool?>(
                  //   context: context,
                  //   builder: (context) => AlertDialog(
                  //     title: Text("Test"),
                  //     content: Text("is it ok?"),
                  //     actions: [
                  //       TextButton(
                  //         onPressed: () {
                  //           Navigator.pop(context, true);
                  //         },
                  //         child: Text("Yes"),
                  //       ),
                  //       TextButton(
                  //         onPressed: () {
                  //           Navigator.pop(context, false);
                  //         },
                  //         child: Text("No"),
                  //       ),
                  //     ],
                  //   ),
                  // );

                  // debugPrint(result.toString());

                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //       builder: (context) => GalleryPage(),
                  //     ));

                  // pushPage(context, GalleryPage());

                  Navigator.pushNamed(context, "gallery", arguments: {
                    "title": "Gallery",
                  });
                },
                child: Container(
                  padding: const EdgeInsets.all(12.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    gradient: LinearGradient(
                      // begin: Alignment(0, 0),
                      // end: Alignment(0, 50),
                      // stops: [0.2, 1],
                      colors: [
                        tkcolor[900]!,
                        tkcolor,
                      ],
                    ),
                  ),
                  child: Icon(
                    Icons.more_vert,
                    size: fltBtnSize,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BAPaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path main = Path();
    main.moveTo(0, 0);
    main.addRRect(RRect.fromRectAndRadius(
      Rect.fromLTWH(-8.0, -2.0, size.width + 16.0, size.height + 4.0),
      const Radius.circular(200),
    ));

    Path slice = Path();
    slice.moveTo(size.width, size.height);
    slice.addOval(Rect.fromCircle(
      center: Offset(size.width + 16, size.height / 2),
      radius: 50,
    ));

    final Path bgPath = Path.combine(PathOperation.difference, main, slice);

    canvas.drawPath(
        bgPath,
        Paint()
          ..color = Colors.black26
          ..maskFilter = MaskFilter.blur(BlurStyle.normal, 5.0));
    canvas.drawPath(bgPath, Paint()..color = Colors.white);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class BAClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path main = Path();
    main.moveTo(0, 0);
    main.addRect(
        Rect.fromLTWH(-16.0, -8.0, size.width + 32.0, size.height + 16.0));

    Path slice = Path();
    slice.moveTo(size.width, size.height);
    slice.addOval(Rect.fromCircle(
      center: Offset(size.width - 8.0, size.height / 2),
      radius: 40,
    ));

    return Path.combine(PathOperation.difference, main, slice);
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }
}

class BNButton extends StatelessWidget {
  final Widget icon;
  final String label;
  final Function(int) onTap;
  final int index;
  final int selectedIndex;

  const BNButton({
    Key? key,
    required this.icon,
    required this.label,
    required this.onTap,
    required this.index,
    required this.selectedIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () => onTap(index),
          // padding: const EdgeInsets.all(0.0),
          child: Container(
            width: 30,
            height: 30,
            child: IconTheme.merge(
              data: IconThemeData(
                size: 30,
                color: selectedIndex == index ? Colors.blue : Colors.grey[700],
              ),
              child: icon,
            ),
          ),
          // iconSize: 30,
          // color: selectedIndex == index ? Colors.blue : Colors.grey[700],
        ),
        Text(label),
      ],
    );
  }
}
