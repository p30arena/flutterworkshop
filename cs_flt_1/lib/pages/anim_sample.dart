import 'package:flutter/material.dart';

class AnimationSamplePage extends StatefulWidget {
  const AnimationSamplePage({Key? key}) : super(key: key);

  @override
  _AnimationSamplePageState createState() => _AnimationSamplePageState();
}

class _AnimationSamplePageState extends State<AnimationSamplePage>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 3));
    animation = Tween<double>(begin: 0, end: 300).animate(controller)
      ..addListener(() {
        setState(() {});
      });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          color: Colors.red,
          width: animation.value,
          height: animation.value,
        ),
      ),
    );
  }
}
