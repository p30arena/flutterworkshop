export 'home.dart';
export 'shop_home.dart';
export 'gallery.dart';
export 'checkout_form.dart';
export 'notifications.dart';
export 'sample_tabbar.dart';
export 'sliver_sample.dart';
export 'anim_sample.dart';
export 'hero_sample.dart';

import 'package:flutter/material.dart';

pushPage(BuildContext context, Widget p) => Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => p,
    ));
