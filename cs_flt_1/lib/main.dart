import 'package:cs_flt_1/core/notifications.dart';
import 'package:flutter/material.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:provider/provider.dart';

import 'core/colors.dart';
import 'pages/pages.dart';

late NotificationsProvider notifp;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  FirebaseMessaging messaging = FirebaseMessaging.instance;

  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );

  // use the returned token to send messages to users from your custom server
  String? token = await messaging.getToken(
      vapidKey:
          "BNYbsvkDlcDJwoqL6IQwkpuWUpAWNTHBoQmkQFh0YhSqHbs71Xe3cqEzKwy0LOWY7FxnZvh_XJs5bWBUhMJn87Q");

  debugPrint(token);

  notifp = NotificationsProvider();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: notifp),
      ],
      builder: (context, _) => MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: tkcolor,
          appBarTheme: AppBarTheme(
            color: tkcolor.shade800,
            titleTextStyle: const TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
            iconTheme: const IconThemeData(
              color: Colors.white,
            ),
          ),
          fontFamily: "Yekan",
        ),
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale("en", ""),
          Locale("fa", ""),
        ],
        locale: const Locale("fa", ""),
        // home: const ShopHomePage(),
        routes: {
          "/": (context) => const ShopHomePage(),
          // "gallery": (context) => const GalleryPage(),
          "form": (context) => const CheckoutFormPage(),
          "sample_tabbar": (context) => const SampleTabbarPage(),
          "sample_sliver": (context) => const SliverSamplePage(),
          "sample_anim": (context) => const AnimationSamplePage(),
          "sample_hero": (context) => HeroAnimationPage(),
        },
        onGenerateRoute: (rs) {
          if (rs.name == "gallery") {
            if (rs.arguments != null) {
              return MaterialPageRoute(
                  builder: (context) => GalleryPage(
                        title: (rs.arguments as Map)["title"],
                      ));
            } else {
              return MaterialPageRoute(
                  builder: (context) => const GalleryPage());
            }
          }
        },
      ),
    );
  }
}
